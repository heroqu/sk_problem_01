const assert = require('assert')
const memoize = require('./memoize')
const { digestInt32Arr } = require('./hash')
const { int32ArrToBufLE } = require('./byteops')

const nonRepeatableFn = function() {
  let count = 0
  return function(arg) {
    return count++
  }
}

describe('Memoize with TTL', () => {
  let f

  beforeEach('Initialize memoizator', () => {
    f = memoize(nonRepeatableFn(), 25)
  })

  it('should return memoized value on similar array', function() {
    const a = [1, 2]
    const b = [1, 2]
    assert.equal(f(a), f(b))
  })

  it('should return new value on similar but new object', function() {
    const a = { x: 1, y: 2 }
    const b = { x: 1, y: 2 }
    assert.notEqual(f(a), f(b))
  })

  it('should return new value after ttl is expired', async function() {
    const a = [1, 2]
    const b = [1, 2]
    const fa = f(a)
    await new Promise(resolve => {
      setTimeout(() => resolve(), 50)
    })
    const fb = f(b)
    assert.notEqual(fa, fb)
  })
})

describe('Memoize with Round Robin', () => {
  let f

  beforeEach('Initialize memoizator', () => {
    f = memoize(nonRepeatableFn(), 25, 2)
  })

  it('should discard older keys in round robin fashion', function() {
    const o1 = { o: 1 }
    const o2 = { o: 2 }
    const o3 = { o: 3 }

    const results = [o1, o2, o3, o1].map(obj => f(obj))

    assert.notEqual(results[0], results[3])
  })
})

describe('Hash function: digestInt32Arr', () => {
  it('should return correct digests', function() {
    const a1 = [1, 2, 3]
    const a2 = [4294967295, 0]

    assert.equal(digestInt32Arr(a1), '85224925b55505af39f1d96a63eb5242')
    assert.equal(digestInt32Arr(a2), '32580e19ef0bc00982c15eedf4b841bd')
  })
})

describe('Byte operations: int32ArrToBufLE', () => {
  it('should return correct buffer', function() {
    const a1 = [1, 2, 3]
    const a2 = [4294967295, 0]

    assert.equal(
      int32ArrToBufLE([1, 2, 3]).toString('hex'),
      '010000000200000003000000',
      'wrong Buffer result'
    )

    assert.equal(
      int32ArrToBufLE([4294967295, 0]).toString('hex'),
      'ffffffff00000000',
      'wrong Buffer result'
    )
  })
})
