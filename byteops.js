/**
 * genInt32ArrToBytesLE - генератор байтов на основе
 *  массива int32 (UInt32/Int32). Для каждого значения
 *  Integer генерирует 4 отдельных байтовых значения.
 *  Порядок байтов little-endian.
 *
 * @param  {[int32]} int32Arr - массив целых чисел (UInt32/Int32)
 * @return {*function}        - генератор
 */
function* genInt32ArrToBytesLE(int32Arr) {
  for (let i of int32Arr) {
    yield i & 0xff
    yield (i >> 8) & 0xff
    yield (i >> 16) & 0xff
    yield (i >> 24) & 0xff
  }
}

/**
 * int32ArrToBufLE - переводит Int32 массив в буфер LE
 *
 * @param  {[int32]} int32Arr - массив целых чисел (UInt32/Int32)
 * @return {Buffer}           - буфер
 */
function int32ArrToBufLE(int32Arr) {
  return Buffer.from(Array.from(genInt32ArrToBytesLE(int32Arr)))
}

module.exports = { int32ArrToBufLE }
