const { MetroHash128 } = require('metrohash')
const { int32ArrToBufLE } = require('./byteops')

const SEED = 123

// размер полученного хэша, зависит от выбора алгоритма
// (например, для MetroHash64 это было бы 8)
const DIGEST_SIZE = 16

/**
 * digestInt32Arr - хэширует Int32/UInt32 массив
 *        используя выбранный алгоритм
 *
 * @param  {[int32]} int32Arr - Int32/UInt32 массив
 * @return {String}           - хэш
 */
function digestInt32Arr(int32Arr) {
  const buf = int32ArrToBufLE(int32Arr)
  const hash = new MetroHash128(SEED)
  hash.update(buf)
  return hash.digest()
}

module.exports = { digestInt32Arr, DIGEST_SIZE }
