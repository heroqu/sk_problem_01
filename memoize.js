const { digestInt32Arr, DIGEST_SIZE } = require('./hash')

// время жизни ключа в кэше по умолчанию
const DEFAULT_TTL = 60000

// Ограничиваем потребление памяти 5Mb
const MEMORY = 5 * 1024 * 1024 // what is available

//  (здесь и далее оценки расхода на hashtable взяты отсюда)
// https://stackoverflow.com/questions/1425221/how-much-memory-does-a-hashtable-use

// размер ссылки в байтах
// Берем 64bit архитектуру как худший случай
const REF_SIZE = 8

// Полный расход памяти на одну запись в Map
const BYTES_PER_ENTRY =
  // внутренние инфрастуктурные расходы на одну запись
  // в Map (без учета самих ключа и значения)
  56 +
  // размер ключа: максимум(размер ссылки, длина хэша)
  // если это Объект, то ссылка, если массив - хэш
  // берем худшее из двух
  Math.max(REF_SIZE, DIGEST_SIZE) +
  // размер значения: считаем что это не больше чем ссылка или число
  REF_SIZE

// Расходы на собственно сам Map как таковой
const MAP_OVERHEAD = 36

// оценка максимального количества записей который можно безопасно
// поместить в кэш
const MAX_ENTRIES = Math.floor((MEMORY - MAP_OVERHEAD) / BYTES_PER_ENTRY)

/**
 * digest - превращает параметр в значение удобное для использования
 *    в качестве ключа Кэша. Объект остается как есть, в то время как
 *    массив из UInt32 заменяется на хэш - строку фиксированной длины
 *    (зависит от выбора хэш алгоритма)
 *
 * @param  {Object|[UInt32]} arg - параметр мемоизируемой функции
 * @return {Object|String}       - подготовленный на его основе ключ
 */
function digest(arg) {
  if (Array.isArray(arg)) {
    return digestInt32Arr(arg)
  }
  return arg
}

/**
 * memoize - момоизатор
 *
 * @param  {Function} fn        - исходная функция
 * @param  {Number} ttl         - время жизни ключа ()
 * @param  {Number} maxEntries  - максимальное число ответов в кэше
 * @return {Function}           - мемоизированная функция
 */
function memoize(fn, ttl = DEFAULT_TTL, maxEntries = MAX_ENTRIES) {
  const m = new Map()

  return function(arg) {
    const dArg = digest(arg)
    const memo = m.get(dArg)
    const ts = Date.now()
    let value

    if (memo && ts < memo.ts + ttl) {
      // имеется значение в кэше, еще не устаревшее
      value = memo.value
    } else {
      // вычисляем значение напрямую
      value = fn(arg)

      if (m.size >= maxEntries) {
        // достигнуто максимально допустимое число записей
        // удаляет одну из старых (самую старую, хотя это
        // и не гарантировано спецификацией языка)
        m.delete(m.keys().next().value)
      }

      // помещаем его в кэш
      m.set(dArg, { ts, value })
    }

    return value
  }
}

module.exports = memoize
